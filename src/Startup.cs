﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SharpRaven;
using SharpRaven.Data;

namespace PingCount
{
    public class Startup
    {
        // public Startup(IConfiguration configuration)
        // {
        //     Configuration = configuration;

        //     var dsn = Environment.GetEnvironmentVariable("SENTRY_DSN");

        //     Console.WriteLine("DSN Sentry : " + dsn);

        //     var ravenClient = new RavenClient(dsn);

        //     try
        //     {
        //         int i2 = 0;
        //         int i = 10 / i2;
        //     }
        //     catch (Exception exception)
        //     {
        //         Console.WriteLine("Logging test exception");
        //         ravenClient.Capture(new SentryEvent(exception));
        //     }
        // }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
