if [ ! $(curl -s $1/count | jq -r '.pingCount') -eq 0 ]
then
  echo "/count invalide"
  exit 1
fi
if [ ! $(curl -s $1/ping | jq -r '.message')=="pong" ]
then
  echo "/ping invalide"
  exit 1
fi
if [ ! $(curl -s $1/count | jq -r '.pingCount') -eq 1 ]
then
  echo "/count invalide"
  exit 1
fi

echo "Tout est ok"
exit 0
