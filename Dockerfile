# Stage 1
FROM microsoft/aspnetcore-build
WORKDIR /source

COPY src .
RUN dotnet restore
RUN dotnet publish --output /app/ --configuration Release

# Stage 2
FROM microsoft/aspnetcore
ENV ASPNETCORE_URLS http://+:80
WORKDIR /app

COPY --from=builder /app .
ENTRYPOINT ["dotnet", "PingCount.dll"]

EXPOSE 80