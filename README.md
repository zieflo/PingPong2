[![pipeline status](https://gitlab.com/MushuLeDragon/PingPong2/badges/master/pipeline.svg)](https://gitlab.com/MushuLeDragon/PingPong2/commits/master)

# PingCount

## Contributeurs

- Loïc TWK
- Floriane Ziégelé

## Dépendances

- Aller sur https://github.com/dotnet/core/releases
- Cliquer sur *Download and Install*
- Télécharger la version SDK souhaitée

## Build & Run

- Ouvrir l'invite de commande dans le dossier et taper : 

```cmd
cd src
dotnet restore
SENTRY_DSN=https://2240a0346bcf4ede8b825286e4c7742c:37b507618b9e42e3802a75e27db2b0af@sentry.io/257984
dotnet run
```

## Test de l'application

./test.sh http://127.0.0.1:63705/ping


*Created by zieflo & MushuLeDragon*
